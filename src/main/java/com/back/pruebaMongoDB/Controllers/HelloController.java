package com.back.pruebaMongoDB.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping("/")
    public String index(){

        return "Hola mundo, probando API back";
    }

    @RequestMapping("/hola")
    public String hello(@RequestParam(value="name", defaultValue="tech API")String name){
        System.out.println("nombre: "+name);
        return String.format("hola %s!", name);
    }


    //ProductController
    //ProductService
    //ProductRepository
    //Querys
    //Conexión a BD(MongoDB, MySQL,etc)
    //ProductModel
}
