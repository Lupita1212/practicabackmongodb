package com.back.pruebaMongoDB.Controllers;


import com.back.pruebaMongoDB.Models.PurchaseModel;
import com.back.pruebaMongoDB.Services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apiBack/v3")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchaseBody){
        System.out.println("addPurchase");
        System.out.println("El id de la compra es: "+ purchaseBody.getId());
        System.out.println("El monto de la compra es: "+ purchaseBody.getAmount());
        System.out.println("El id de la compra es: "+ purchaseBody.getPurchaseItems());
        System.out.println("El id de la compra es: "+ purchaseBody.getUserId());

        return new ResponseEntity<>(
                this.purchaseService.add(purchaseBody),
                HttpStatus.CREATED
        );
    }
}
