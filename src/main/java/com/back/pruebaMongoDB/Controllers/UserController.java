package com.back.pruebaMongoDB.Controllers;


import com.back.pruebaMongoDB.Models.UserModel;
import com.back.pruebaMongoDB.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apiBack/v2")
public class UserController {
    @Autowired
    UserService userService;


    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam( name="$orderBy", required= false) String orderBy){
        System.out.println("getUsers");
        System.out.println("El orderBY es: "+ orderBy);


        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUsers(@RequestBody UserModel userModel){
        System.out.println("addUser");

        System.out.println("El id del producto es: " +userModel.getId());
        System.out.println("La descripcion producto es: " +userModel.getName());
        System.out.println("La descripcion producto es: " +userModel.getAge());
        return new ResponseEntity<>(this.userService.add(userModel),HttpStatus.CREATED);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUser(@PathVariable String id){
        System.out.println("getUser");
        System.out.println("El id a buscar es: "+id);
        Optional<UserModel> result= this.userService.findById(id);
        return new ResponseEntity<>(
                result.isPresent() ? result.get() :" user no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel userBody,@PathVariable String id){
        System.out.println("updateUser");

        System.out.println("El id del user para actualizar en el parametro de la URL es: "+id);
        System.out.println("El id de usuario actualizar es: "+userBody.getId());
        System.out.println("El nombre de usuario actualizar es: "+userBody.getName());
        System.out.println("La edad de usuario actualizar es: "+userBody.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);
        if (userToUpdate.isPresent()) {
            System.out.println("El usuario actualizar es: "+ id);
            this.userService.update(userBody);
        }
        return new ResponseEntity<>(
                userBody, userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id){
        System.out.println("deleteUser");
        boolean bandera= this.userService.delete(id);
        return new ResponseEntity<>(
                bandera ? "Usuario eliminado" : "Usuario no encontrado",
                bandera ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
