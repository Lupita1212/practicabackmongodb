package com.back.pruebaMongoDB.Models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document( collection ="Purchases")
public class PurchaseModel {

    @Id
    private String id;
    private String userId;
    private Float amount;
    private int purchaseItems;

    public PurchaseModel() {
    }

    public PurchaseModel(String id, String userId, Float amount, int purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public int getPurchaseItems() {
        return purchaseItems;
    }

    public void setPurchaseItems(int purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
