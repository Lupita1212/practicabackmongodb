package com.back.pruebaMongoDB.Repositories;

import com.back.pruebaMongoDB.Models.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductModel, String>{


}
