package com.back.pruebaMongoDB.Repositories;

import com.back.pruebaMongoDB.Models.PurchaseModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
