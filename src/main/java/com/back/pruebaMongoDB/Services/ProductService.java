package com.back.pruebaMongoDB.Services;

import com.back.pruebaMongoDB.Models.ProductModel;
import com.back.pruebaMongoDB.Repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll(){
        System.out.println("findAll productService");
    return this.productRepository.findAll();

    }
    public ProductModel add(ProductModel product){
        System.out.println("add en productService");
        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product){
        System.out.println("update en productService");
        return this.productRepository.save(product);
    }

    public Optional<ProductModel> findById (String id){
        System.out.println("findById en productService");

        return this.productRepository.findById(id);

    }

    public boolean delete(String id){
        boolean result=false;
        System.out.println("deleteProduct productService");
        if (this.findById(id).isPresent()== true){
            System.out.println("producto encontrado y eliminado");
            result=true;
            this.productRepository.deleteById(id);
        }
    return result;
    }
}
