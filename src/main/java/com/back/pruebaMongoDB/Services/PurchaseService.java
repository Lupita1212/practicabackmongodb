package com.back.pruebaMongoDB.Services;

import com.back.pruebaMongoDB.Models.PurchaseModel;
import com.back.pruebaMongoDB.Repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PurchaseService {
    @Autowired
    PurchaseRepository purchaseRepository;

    public PurchaseModel add(PurchaseModel purchaseBody) {
        System.out.println("add en purchaseService");
        return this.purchaseRepository.save(purchaseBody);

    }
}
