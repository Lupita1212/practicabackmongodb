package com.back.pruebaMongoDB.Services;

import com.back.pruebaMongoDB.Models.UserModel;
import com.back.pruebaMongoDB.Repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public List<UserModel> findAll(String orderBy){
        System.out.println("findAll de userService");
        List<UserModel> result;
        if (orderBy !=null){

            result=this.userRepository.findAll(Sort.by("age"));
        }else{
            result=this.userRepository.findAll();
        }
        return result;
    }

    public UserModel add(UserModel userModel) {
        System.out.println("addUser en userService");

        return this.userRepository.save(userModel);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en userService");
        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel) {
        System.out.println("update en userService");
        return this.userRepository.save(userModel);
    }

    public boolean delete(String id) {
        System.out.println("Eliminar en userService");
        boolean bandera =false;
        if (this.findById(id).isPresent()==true){
            System.out.println("Usuario encontrado y eliminando");
            bandera=true;
            this.userRepository.deleteById(id);
        }
        return bandera;
    }
}
